/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */

package ru.erlinve.currency.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ru.erlinve.currency.Logger;
import ru.erlinve.currency.R;
import ru.erlinve.currency.activity.MainActivity;
import ru.erlinve.currency.parcels.ValuteDataParcel;
import ru.erlinve.currency.parcels.ValuteDinamicParcel;

public class DinamicValuteFragment extends Fragment implements View.OnClickListener, DatePickerFragment.DatePickerDialogListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = DinamicValuteFragment.class.getName();
    private static final String BUNDLE_KEY_VALUTE_HEADER = "bundlekeyvaluteheader";
    private static final String BUNDLE_KEY_VALUTE_DINAMIC = "bundlekeyvalutedinamic";
    private static final String DATE_LABEL_FORMAT = "%1$td.%1$tm.%1$tY";

    private int mDatePickerCallerId = 0;
    private long mDateStart = 0;
    private long mDateEnd = 0;

    private ValuteDataParcel mValuteDataParcel;
    private ArrayList<ValuteDinamicParcel> mValuteDinamicParcelArray;

    private DiagramView mDiagramView;
    private ImageButton mLogoButton;
    private Button mFlagButton;
    private TextView mNominalText;
    private Button mStartCalendarButton;
    private Button mEndCalendarButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private DatePickerFragment mDialogFragment;

    public DinamicValuteFragment() {
        // Required empty public constructor
    }

    public static DinamicValuteFragment newInstance(ValuteDataParcel parcelHeader) {
        Logger.debug(TAG, "NewInstance");
        DinamicValuteFragment fragment = new DinamicValuteFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_KEY_VALUTE_HEADER, parcelHeader);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.debug(TAG, "On Create");
        if (getArguments() != null) {
            mValuteDataParcel = getArguments().getParcelable(BUNDLE_KEY_VALUTE_HEADER);
        }
        if(savedInstanceState!=null)
        {
            mValuteDinamicParcelArray = savedInstanceState.getParcelableArrayList(BUNDLE_KEY_VALUTE_DINAMIC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Logger.debug(TAG, "On Create View");
        View view = inflater.inflate(R.layout.fragment_dinamic_valute, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_din);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setSize(SwipeRefreshLayout.LARGE);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(true);

        if(mValuteDataParcel!=null) {
            mDialogFragment = new DatePickerFragment();
            mDialogFragment.setParent(this);

            mDiagramView = (DiagramView) view.findViewById(R.id.diagramViewId);

            mLogoButton = (ImageButton) view.findViewById(R.id.logoButton);
            mFlagButton = (Button) view.findViewById(R.id.flagButton);
            mFlagButton.setOnClickListener(this);
            mNominalText = (TextView) view.findViewById(R.id.nomText);

            mStartCalendarButton = (Button) view.findViewById(R.id.buttonStartCalendar);
            mStartCalendarButton.setOnClickListener(this);
            mEndCalendarButton = (Button) view.findViewById(R.id.buttonEndCalendar);
            mEndCalendarButton.setOnClickListener(this);

            this.setDateRange((mValuteDataParcel.getDateUp()-2592000000L), mValuteDataParcel.getDateUp());
            this.setNominalText(mValuteDataParcel.getNominal());
            Drawable d = this.getDrawableFlag(mValuteDataParcel.getCharCode());
            mFlagButton.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);
            mFlagButton.setText(mValuteDataParcel.getCharCode());

        }
        else {
            Logger.event(TAG, "ValuteParcel==NULL");
        }
        mSwipeRefreshLayout.setRefreshing(true);
        if(mValuteDinamicParcelArray!=null)
        {
            this.createGraphic(mValuteDinamicParcelArray, true);
        }
        else {
            this.downloadParcelList();
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Logger.debug(TAG, "On Save Instance State");
        outState.putParcelableArrayList(BUNDLE_KEY_VALUTE_DINAMIC, mValuteDinamicParcelArray);
        super.onSaveInstanceState(outState);
    }

    public void createGraphic(ArrayList<ValuteDinamicParcel> valuteDataParcelArrayList, boolean fullRange) {

        if(!fullRange)
        {
            MsgDialogFragment dialogFragment = new MsgDialogFragment().newInstance(MsgDialogFragment.RANGE_NOT_FULL);
            dialogFragment.show(getFragmentManager(), "dialogFragment");
            Logger.debug(TAG, "! FULL RANGE");
        }
        Logger.debug(TAG, "Create Graphic");
        Logger.debug(TAG, valuteDataParcelArrayList.get(0).toString());
        if(valuteDataParcelArrayList.size()>1) {
            this.setDateRange(valuteDataParcelArrayList.get(0).getDateUp(), valuteDataParcelArrayList.get(valuteDataParcelArrayList.size() - 1).getDateUp());
        }
        int nominal = valuteDataParcelArrayList.get(valuteDataParcelArrayList.size()-1).getNominal();
        this.setNominalText(nominal);

        mValuteDinamicParcelArray = valuteDataParcelArrayList;
        mDiagramView.getValuteDataParcel(valuteDataParcelArrayList);
        mDiagramView.invalidate();

        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.buttonStartCalendar :
                mDatePickerCallerId = v.getId();
                mDialogFragment.show(getFragmentManager(), "DATEPICKER");
                break;
            case R.id.buttonEndCalendar :
                mDatePickerCallerId = v.getId();
                mDialogFragment.show(getFragmentManager(), "DATEPICKER");
                break;
            case R.id.logoButton :
            case R.id.flagButton :
                this.getFragmentManager().popBackStack();
            default:
                break;
        }

    }

    @Override
    public void onFinishEditDialog(long date) {

        mSwipeRefreshLayout.setRefreshing(true);

        switch (mDatePickerCallerId)
        {
            case R.id.buttonStartCalendar :

                Logger.debug(TAG, " button End set date"+String.format("%1$td_%1$tm_%1$tY", date));
                this.setDateRange(date, mDateEnd);
                this.downloadParcelList();
                break;
            case R.id.buttonEndCalendar :

                Logger.debug(TAG, " button Start set date"+String.format("%1$td_%1$tm_%1$tY", date));
                this.setDateRange(mDateStart, date);
                this.downloadParcelList();
                break;
            default:
                mSwipeRefreshLayout.setRefreshing(false);
                break;
        }
    }

    private void setDateRange(long start, long end) {

        if(mEndCalendarButton != null && mStartCalendarButton != null) {
            Date dateEnd = new Date(end);
            Date dateStart = new Date(start);
            mEndCalendarButton.setText(String.format(Locale.getDefault(), DATE_LABEL_FORMAT, dateEnd));
            mStartCalendarButton.setText(String.format(Locale.getDefault(), DATE_LABEL_FORMAT, dateStart));
        }
        mDateEnd = end;
        mDateStart = start;
    }

    private void setNominalText(int nominal)
    {
        if(nominal!=1)
        {
            mNominalText.setText("for:"+String.valueOf(nominal));
            mNominalText.setVisibility(View.VISIBLE);
        }
        else
        {
            mNominalText.setVisibility(View.GONE);
        }
    }

    private void downloadParcelList() {

        Logger.debug(TAG, "Download Parcel List");
        MainActivity mainActivity = (MainActivity) this.getActivity();
        mainActivity.downloadValuteDinamic(mValuteDataParcel, mDateStart, mDateEnd);
    }

    private Drawable getDrawableFlag( String charCode)
    {
        Drawable drawable;
        Context context = this.getActivity().getBaseContext();

        int drawableId = context.getResources()
                .getIdentifier(charCode.toLowerCase(),
                        "drawable", context.getPackageName());
        if(drawableId==0) {
/** IF THERE ARE NO FLAG FOR THIS XML-CHARCODE */
            Logger.event(TAG, "there are no flag for this xml charcode: "
                    +mValuteDataParcel.getCharCode()+" xml-date: "
                    + new Date( mValuteDataParcel.getDateUp()).toString());

            drawableId = context.getResources()
                    .getIdentifier("xxx",
                            "drawable", context.getPackageName());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable = context.getDrawable(drawableId);
        }
        else {
            drawable = context.getResources().getDrawable(drawableId);
        }
        return drawable;
    }

    @Override
    public void onRefresh() {

        this.setDateRange((mValuteDataParcel.getDateUp()-2592000000L), mValuteDataParcel.getDateUp());
        this.downloadParcelList();
    }
}
