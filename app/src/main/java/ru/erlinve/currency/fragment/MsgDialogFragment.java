/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ru.erlinve.currency.R;

public class MsgDialogFragment  extends DialogFragment {

    private static final String TAG = MsgDialogFragment.class.getName();
    private static final String CONTENT_TAG = "content";
    private static final String TITLE_INTERNET_ERROR = "ERROR INTERNET CONNECTION";
    private static final String TITLE_RANGE_NOT_FULL = "This currency had been modified in that period";
    private static final String BUTTON_TITLE_RESUME = "resume";
    private static final String BUTTON_TITLE_CLOSE = "close";
    private static final String BUTTOM_TITLE_OK = "OK";

    public static final int INTERNET_ERROR = 0;
    public static final int RANGE_NOT_FULL = 1;

    public static MsgDialogFragment newInstance(int content) {
        MsgDialogFragment frag = new MsgDialogFragment();
        Bundle args = new Bundle();
        args.putInt(CONTENT_TAG, content);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int context = getArguments().getInt(CONTENT_TAG);

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setIcon(R.mipmap.ic_launcher);

        switch (context) {

            case INTERNET_ERROR :

                dialog.setTitle(TITLE_INTERNET_ERROR);
                dialog.setPositiveButton(BUTTON_TITLE_RESUME,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dismiss();
                                getActivity().recreate();
                            }
                        }
                )
                        .setNegativeButton(BUTTON_TITLE_CLOSE,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        getActivity().finish();
                                    }
                                }
                        );
                break;
            case RANGE_NOT_FULL :
                dialog.setTitle(TITLE_RANGE_NOT_FULL);
                dialog.setNeutralButton((BUTTOM_TITLE_OK), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface di, int whichButton) {

                    }
                });
                break;

            default:
                break;
        }

        return dialog.create();
    }

}
