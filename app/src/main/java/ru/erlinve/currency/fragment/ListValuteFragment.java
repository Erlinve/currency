/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;

import ru.erlinve.currency.Logger;
import ru.erlinve.currency.R;
import ru.erlinve.currency.activity.MainActivity;
import ru.erlinve.currency.parcels.ValuteDataParcel;

public class ListValuteFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
                                                            View.OnClickListener,
                                                DatePickerFragment.DatePickerDialogListener {

    private static final String TAG = ListValuteFragment.class.getName();
    private static final String LIST_PARCEL_KEY = "listparcelkey";

    private static final String DATE_LABEL_FORMAT = "%1$td.%1$tm.%1$tY";

    private ArrayList<ValuteDataParcel> mDataParcelArrayList;
    private RecyclerView mValRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Button mCalendarButton;
    private ImageButton mLogoButton;
    private DatePickerFragment mDialogFragment;


    public ListValuteFragment() {
        // Required empty public constructor
    }

    public static ListValuteFragment newInstance(List<ValuteDataParcel> dataParcelList) {

        ListValuteFragment fragment = new ListValuteFragment();
        Bundle args = new Bundle();

        args.putParcelableArrayList(LIST_PARCEL_KEY, (ArrayList<? extends Parcelable>) dataParcelList);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mDataParcelArrayList = getArguments().getParcelableArrayList(LIST_PARCEL_KEY);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_valute, container, false);

        mValRecyclerView = (RecyclerView) view.findViewById(R.id.listValuteView);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mCalendarButton = (Button) view.findViewById(R.id.buttonListCalendar);
        mCalendarButton.setOnClickListener(this);
        mLogoButton = (ImageButton) view.findViewById(R.id.logoButton);
        mLogoButton.setOnClickListener(this);

        mLayoutManager = new LinearLayoutManager(getActivity());

        mDialogFragment = new DatePickerFragment();
        mDialogFragment.setParent(this);

        if(mDataParcelArrayList!=null) {
            mCalendarButton.setText(new Formatter().format(DATE_LABEL_FORMAT, mDataParcelArrayList.get(0).getDateUp()).toString());

            ValListAdapter valListAdapter = new ValListAdapter(mDataParcelArrayList);
            mValRecyclerView.setAdapter(valListAdapter);
            mValRecyclerView.setLayoutManager(mLayoutManager);
            mValRecyclerView.setOnScrollChangeListener(new RecyclerView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    mSwipeRefreshLayout.setEnabled(mLayoutManager.findViewByPosition(0) != null);
                }
            });
        }
        else {
            Logger.error(TAG, "mDataParcelArrayList is NULL", new Throwable("mDataParcelArrayList is NULL"));
        }
        return view;
    }

    @Override
    public void onRefresh() {

        mSwipeRefreshLayout.setRefreshing(true);
        MainActivity activity = (MainActivity) getActivity();
        activity.downloadValuteList(new Date().getTime());

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.logoButton :
                this.getFragmentManager().popBackStack();
                break;
            case R.id.buttonListCalendar :
                mDialogFragment.show(getFragmentManager(), "DATEPICKER");
                break;
            default:
                break;
        }
    }

    @Override
    public void onFinishEditDialog(long date) {

        mSwipeRefreshLayout.setRefreshing(true);
        mCalendarButton.clearComposingText();

        mCalendarButton.setText(new Formatter().format(DATE_LABEL_FORMAT, date).toString());

        MainActivity activity = (MainActivity) getActivity();
        activity.downloadValuteList(date);
    }
}
