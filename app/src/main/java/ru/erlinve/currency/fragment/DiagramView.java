/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.fragment;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;

import ru.erlinve.currency.Logger;
import ru.erlinve.currency.parcels.ValuteDinamicParcel;

public class DiagramView extends View {

    private static final String TAG = DiagramView.class.getName();
    private static final String VALUE_FORMAT = "%.4f";
    private static final String SHORT_TITLE = "TOO SHORT RANGE";
    private static final String FULL_DATE_FORMAT = "%1$td.%1$tm.%1$tY";
    private static final String SHORT_VAL_FORMAT = "VALUE: %1$f (RUB)";
    private static final String NOMINAL_FORMAT = "FOR: %1$d";
    private static final String DM_DATE_FORMAT = "%1$td.%1$tm";
    public static final String MY_DATE_FORMAT = "%1$tm/%1$ty";

    private ArrayList<ValuteDinamicParcel> mValiteParcelList;

    private Paint mPaintText;
    private Paint mPaintLines;

    private float mDensity = 1; //calculate in custCreate()
    private final static int mBigTextSize = 20;
    private final static int mSmallTextSize = 14;

    public DiagramView(Context context) {
        super(context);
        this.custCreate();
    }

    public DiagramView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.custCreate();
    }

    public DiagramView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.custCreate();
    }


    @Override
    protected void onDraw(Canvas canvas) {

// frame Width & Height - parameters of the DiagramView
        int frameWidth = getMeasuredWidth();
        int frameHeight = getMeasuredHeight();

        mPaintText.setTextSize(mDensity*mBigTextSize);
        mPaintLines.setColor(Color.GREEN);
        mPaintLines.setStrokeWidth(2);

        if(mValiteParcelList != null)
        {
            if(mValiteParcelList.size() < 1)
            {
                String message = SHORT_TITLE;
                canvas.drawText(message, ((frameWidth - mPaintText.measureText(message))/2),
                                            (frameHeight/2), mPaintText);
            }
            else if(mValiteParcelList.size() == 1)
            {
                ArrayList<String> messageList = new ArrayList<>();
                messageList.add(SHORT_TITLE);
                messageList.add(String.format(Locale.getDefault(), FULL_DATE_FORMAT,
                                                mValiteParcelList.get(0).getDateUp()));
                messageList.add(String.format(Locale.getDefault(), SHORT_VAL_FORMAT,
                                                mValiteParcelList.get(0).getValue()));

                if(mValiteParcelList.get(0).getNominal()>1)
                {
                    messageList.add(String.format(Locale.getDefault(), NOMINAL_FORMAT,
                                                    mValiteParcelList.get(0).getNominal()));
                }
                Rect textBoundRect = new Rect();
                mPaintText.getTextBounds(messageList.get(0), 0, messageList.get(0).length(),
                                            textBoundRect);

                float strHeight = (float)(textBoundRect.height()*1.5);

                for(int i=0; i<messageList.size(); i++)
                {
                    canvas.drawText(messageList.get(i),
                            ((frameWidth - mPaintText.measureText(messageList.get(i)))/2),
                            ((frameHeight/3)+(i*strHeight)), mPaintText);
                }
            }
            else
            {

// higthest & lowest point related with scale of diagram
// strValue set the longest stirng of valute value
                double highest = 0;
                double lowest = mValiteParcelList.get(0).getValue();
                String strValue = "";

                for(ValuteDinamicParcel valuteDataParcel : mValiteParcelList) {

                    if(valuteDataParcel.getValue() > highest) highest = valuteDataParcel.getValue();
                    if(valuteDataParcel.getValue() < lowest) lowest = valuteDataParcel.getValue();
                    if((strValue.length()) < (String.valueOf(valuteDataParcel.getValue()).length()))
                    {
                        strValue = String.format(Locale.getDefault(), VALUE_FORMAT,
                                                    valuteDataParcel.getValue());
                    }
                }

// field width & height - parameters of diagram field
// zero-field-height geted  after text line before the field
                Rect textBoundRect = new Rect();
                mPaintText.getTextBounds(strValue, 0, strValue.length(), textBoundRect);

                float fieldWidth = (float) frameWidth - mPaintText.measureText(strValue) -2*mDensity;
                float zeroFieldHeight = textBoundRect.height();
                float fieldHeight = (float) frameHeight - zeroFieldHeight;

// frame of the field
                canvas.drawRect(0, zeroFieldHeight, fieldWidth, frameHeight, mPaintLines);

// steps related with valute-points on diagram
                double stepHeight = (fieldHeight / (highest-lowest));
                float stepWidth = fieldWidth/(mValiteParcelList.size()-1);

// creating Array of valute-points on diagram
                float[] arrayPoints = new float[mValiteParcelList.size()*4];
                int arrayIndex = 0;

                float beginX = 0;
                float beginY =(float) (stepHeight * (highest - mValiteParcelList.get(0).getValue())
                                                  + zeroFieldHeight);

                for(int i=1; i<mValiteParcelList.size(); i++) {

                    ValuteDinamicParcel valuteDataParcel = mValiteParcelList.get(i);
                    arrayPoints[arrayIndex] = beginX;
                    arrayIndex++;
                    arrayPoints[arrayIndex] = beginY;
                    arrayIndex++;
                    arrayPoints[arrayIndex] = stepWidth * i;
                    beginX = arrayPoints[arrayIndex];
                    arrayIndex++;
                    arrayPoints[arrayIndex] = (float)
                            (stepHeight * (highest - valuteDataParcel.getValue())) + zeroFieldHeight;
                    beginY = arrayPoints[arrayIndex];
                    arrayIndex++;
                }

// Draw diagram lines
                mPaintLines.setColor(Color.RED);
                mPaintLines.setStrokeWidth(2);
                canvas.drawLines(arrayPoints, mPaintLines);

                float textValWidth = fieldWidth+2*mDensity;
// Draw valute values on highest & lowest points
                canvas.drawText(String.format(Locale.getDefault(), VALUE_FORMAT, highest),
                        textValWidth, zeroFieldHeight*2, mPaintText);
                canvas.drawText(String.format(Locale.getDefault(), VALUE_FORMAT, lowest),
                        textValWidth, (frameHeight - 2*mDensity), mPaintText);

// Draw middle value text and horisontal lines
                mPaintLines.setColor(Color.YELLOW);
                mPaintLines.setStrokeWidth(1);
                float lineStep = 60*mDensity;
                for(float i=(lineStep+zeroFieldHeight); i<(frameHeight-lineStep); i+=lineStep)
                {
                    double val = i/stepHeight+lowest;
                    canvas.drawText(String.format(Locale.getDefault(), VALUE_FORMAT, val),
                            textValWidth, frameHeight-i, mPaintText);
                    canvas.drawLine(0, frameHeight-i, fieldWidth, frameHeight-i, mPaintLines);

                }

// Draw middle date text and vertical lines
                if(mValiteParcelList.size()>3) {
                    int valLines = 5;

                    if ((mValiteParcelList.size() % 2) == 0) {
                        valLines = 4;
                    }
                    else if(mValiteParcelList.size()==7)
                    {
                        valLines=3;
                    }


                    int mod = new BigDecimal(String.valueOf((double) mValiteParcelList.size() / valLines))
                                    .setScale(0, BigDecimal.ROUND_HALF_UP)
                                    .intValue();

                    mPaintText.setTextSize(mDensity*mSmallTextSize);
                    String formatting = DM_DATE_FORMAT;
                    if (mValiteParcelList.size() > 100) {
                        formatting = MY_DATE_FORMAT;
                    }

                    for (int i = 1; i < valLines; i++) {
                            float pointWidth = stepWidth*mod*i;
                        if(pointWidth<fieldWidth) {

                            String str = String.format(Locale.getDefault(), formatting,
                                            mValiteParcelList.get((mod * i)).getDateUp());

                            canvas.drawLine(pointWidth, zeroFieldHeight, pointWidth,
                                                frameHeight, mPaintLines);
                            canvas.drawText( str, (pointWidth - mPaintText.measureText(str)/2),
                                    (zeroFieldHeight - 3*mDensity), mPaintText);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 1);
        int minh = getSuggestedMinimumHeight()+ getPaddingBottom() + getPaddingTop();
        int h = resolveSizeAndState(minh, heightMeasureSpec, 0);

        setMeasuredDimension(w, h);
    }

    public void getValuteDataParcel(ArrayList<ValuteDinamicParcel> valuteParcelArrayList)
    {
        mValiteParcelList = valuteParcelArrayList;
    }

    private void custCreate()
    {
        mValiteParcelList = null;
        mPaintText = new Paint();
        mPaintText.setStyle(Paint.Style.FILL);
        mPaintText.setColor(Color.BLUE);
        mPaintLines = new Paint();
        mPaintLines.setStyle(Paint.Style.STROKE);
        mDensity = this.getResources().getDisplayMetrics().density;

        Logger.debug(TAG, "DINSITY IS: "+String.valueOf(mDensity));
    }

}
