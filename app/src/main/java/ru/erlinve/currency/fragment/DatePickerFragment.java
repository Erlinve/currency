/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

import ru.erlinve.currency.R;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = DatePickerFragment.class.getName();

    private Fragment mParentFragment;

    private static final int MIN_YEAR = 1999;
    private static final int MIN_MONTH = 0;
    private static final int MIN_DAY = 1;
    private int mMaxYear;
    private int mMaxMonth;
    private int mMaxDay;

    private Calendar mCurrentCalendar;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mCurrentCalendar = Calendar.getInstance();
        mMaxYear = mCurrentCalendar.get(Calendar.YEAR);
        mMaxMonth = mCurrentCalendar.get(Calendar.MONTH);
        mMaxDay = mCurrentCalendar.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(getActivity(), R.style.MaterialDatePickerTheme,this, mMaxYear, mMaxMonth, mMaxDay);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        if(c.getTimeInMillis() > mCurrentCalendar.getTimeInMillis())
        {
//            year = mMaxYear;
//            monthOfYear = mMaxMonth;
//            dayOfMonth = mMaxDay;
            c = mCurrentCalendar;
        }
        else if(year < MIN_YEAR)
        {
//            year= MIN_YEAR;
//            monthOfYear=0;
//            dayOfMonth=01;

            c.set(MIN_YEAR, MIN_MONTH, MIN_DAY);
        }



        if(mParentFragment!=null) {

            DatePickerDialogListener listener = (DatePickerDialogListener) mParentFragment;
            listener.onFinishEditDialog(c.getTimeInMillis());
        }

        this.dismiss();
    }


    // TODO: 6/27/16 When API 15, 16 will be unsupported use Fragment.getParentFragment
    public void setParent(Fragment fragment) {

        mParentFragment = fragment;
    }

/** LISTENER INTERFACE */

    public interface DatePickerDialogListener {
        void onFinishEditDialog(long date);
    }
}
