/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */

package ru.erlinve.currency.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.erlinve.currency.R;

public class StartFragment extends Fragment {


    public StartFragment() {
        // Required empty public constructor
    }

    public static StartFragment newInstance() {

        StartFragment fragment = new StartFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_start, container, false);
        return v;
    }

}
