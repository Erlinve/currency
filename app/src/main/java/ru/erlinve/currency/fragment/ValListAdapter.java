/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ru.erlinve.currency.Logger;
import ru.erlinve.currency.R;
import ru.erlinve.currency.activity.MainActivity;
import ru.erlinve.currency.parcels.ValuteDataParcel;

public class ValListAdapter extends RecyclerView.Adapter<ValListAdapter.ValuteViewHolder> {

    private static final String TAG = ValListAdapter.class.getName();
    private static final int MAX_FRACTION_DIGITS = 5;

    private ArrayList<ValuteDataParcel> mValuteParcelArrayList;



    public static class ValuteViewHolder extends RecyclerView.ViewHolder {

        private final ImageView flagView;
        private final TextView charcodeView;
        private final TextView nameView;
        private final TextView valueView;
        private final TextView nominalView;

        private Context holderContext;

        private ValuteDataParcel valuteDataParcel;

        public ValuteViewHolder(View itemView) {
            super(itemView);

            holderContext = itemView.getContext();

            flagView = (ImageView) itemView.findViewById(R.id.imageFlag);
            charcodeView = (TextView) itemView.findViewById(R.id.textCharCode);
            nameView = (TextView) itemView.findViewById(R.id.textValuteName);
            valueView = (TextView) itemView.findViewById(R.id.textValuteValue);
            nominalView = (TextView) itemView.findViewById(R.id.textNominalValue);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(valuteDataParcel!=null) {
                        ((MainActivity) holderContext).addDinamicFragment(valuteDataParcel);

                    }
                }
            });

        }

        public void addDataToView(ValuteDataParcel data) {

            valuteDataParcel = data;

            charcodeView.setText(data.getCharCode());
            nameView.setText(data.getName());
            valueView.setText(String.valueOf(data.getValue()));

            if(data.getNominal()>1)
            {
                nominalView.setVisibility(View.VISIBLE);
                String s = "for %d";
                nominalView.setText(String.format(s, data.getNominal()));
            }
            else
            {
                nominalView.setVisibility(View.GONE);
            }

            int id = holderContext.getResources()
                    .getIdentifier(data.getCharCode().toLowerCase(),
                            "drawable", holderContext.getPackageName());

            if(id==0) {
/** IF THERE ARE NO FLAG FOR THIS XML-CHARCODE */
                Logger.event(TAG, "there are no flag for this xml charcode: "
                        +data.getCharCode()+" xml-date: "
                        + new Date( data.getDateUp()).toString());
                id = holderContext.getResources()
                        .getIdentifier("xxx",
                                "drawable", holderContext.getPackageName());
            }

            flagView.setImageResource(id);
        }
    }



    public ValListAdapter(ArrayList<ValuteDataParcel> data) {

        mValuteParcelArrayList = data;
    }
    @Override
    public ValListAdapter.ValuteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ValuteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ValuteViewHolder holder, int position) {

        final ValuteDataParcel valuteData = mValuteParcelArrayList.get(position);

        holder.addDataToView(valuteData);

    }


    @Override
    public int getItemCount() {
        return mValuteParcelArrayList.size();
    }

}