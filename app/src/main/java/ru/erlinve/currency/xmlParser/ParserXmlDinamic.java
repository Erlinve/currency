/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.xmlParser;

import android.net.ParseException;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.erlinve.currency.Logger;
import ru.erlinve.currency.parcels.ValuteDataParcel;
import ru.erlinve.currency.parcels.ValuteDinamicParcel;

public class ParserXmlDinamic {


    private static final String TAG = ParserXmlDinamic.class.getName();
    private static final String TAG_DATE_FORMAT = "dd.MM.yyyy";

    /** tag for xml */

    private static final String TAG_RECORD = "Record";
    private static final String TAG_NOMINAL = "Nominal";
    private static final String TAG_VALUE = "Value";

    public ParserXmlDinamic() { }

    public List<ValuteDinamicParcel> getListValuteParcel (InputStream inputStream) {

        return this.handleXmlStream(inputStream);
    }

    private List<ValuteDinamicParcel> handleXmlStream(InputStream inputStream) {

        List<ValuteDinamicParcel> resultList = new ArrayList<ValuteDinamicParcel>();

        try{
            XmlPullParser parser = Xml.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);
            parser.nextTag();

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {

//CONTAINER_TAG FOR VALUTE DATA-TAGS

                    if(parser.getName().equals(TAG_RECORD))
                    {
                        resultList.add(readRecord(parser));
                    }
                }
                eventType = parser.next();
            }
        }
        catch (XmlPullParserException | IOException | ParseException | java.text.ParseException e) {
            Logger.error(TAG, e);
        }
        return resultList;
    }

    // PARSE VALUTE TAG_CONTAINER
    private ValuteDinamicParcel readRecord (XmlPullParser parser) throws IOException, XmlPullParserException, java.text.ParseException {

        String dateString = parser.getAttributeValue(0);
        SimpleDateFormat newDateFormat = new SimpleDateFormat(TAG_DATE_FORMAT);
        dateString = dateString.replaceAll("/",".");

        long dateUp = newDateFormat.parse(dateString).getTime();

        String id = parser.getAttributeValue(1);

        int nominal = 0;
        double value = 0;

        parser.require(XmlPullParser.START_TAG, null, TAG_RECORD);

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nameParser = parser.getName();

            if (nameParser.equals(TAG_NOMINAL))
            {
                nominal = Integer.parseInt(getTagValue(parser, TAG_NOMINAL));
            }
            else if (nameParser.equals(TAG_VALUE))
            {
                value = Double.parseDouble(getTagValue(parser, TAG_VALUE).replace(",","."));
            }
        }

        return new ValuteDinamicParcel(id, value, nominal, dateUp);
    }

    // RETURN VALUE FOR TAG
    private  String getTagValue(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException {

        String result = "";

        parser.require(XmlPullParser.START_TAG, null, tagName);

        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }

        parser.require(XmlPullParser.END_TAG, null, tagName);
        return result;
    }
}
