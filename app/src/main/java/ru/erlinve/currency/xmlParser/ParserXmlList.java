/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.xmlParser;

import android.net.ParseException;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.erlinve.currency.Logger;
import ru.erlinve.currency.parcels.ValuteDataParcel;

public class ParserXmlList {


    private static final String TAG = ParserXmlList.class.getName();
    private static final String TAG_DATE_FORMAT = "dd.MM.yyyy";

// tags of xml

    private static final String TAG_VALCURS = "ValCurs";
    private static final String TAG_VALUTE = "Valute";
    private static final String TAG_NUMCODE = "NumCode";
    private static final String TAG_CHARCODE = "CharCode";
    private static final String TAG_NOMINAL = "Nominal";
    private static final String TAG_NAME_VALUTE = "Name";
    private static final String TAG_VALUE = "Value";

    private long mDateUp = new Date().getTime();

    public ParserXmlList() { }

    public List<ValuteDataParcel> getListValuteParcel (InputStream inputStream) {

        return this.handleXmlStream(inputStream);
    }

    private List<ValuteDataParcel> handleXmlStream(InputStream inputStream) {

        List<ValuteDataParcel> resultList = new ArrayList<ValuteDataParcel>();

        try{
            XmlPullParser parser = Xml.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);
            parser.nextTag();

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {

//CONTAINER_TAG FOR VALUTE DATA-TAGS

                    if(parser.getName().equals(TAG_VALUTE))
                    {
                        resultList.add(readValute(parser));
                    }

// ROOT TAG OF DOCUMENT

                    else if(parser.getName().equals(TAG_VALCURS))
                    {
// EXTRACT DATE OF PUBLISHING XML-DOCUMENT
// (if our specified date is weekend it may not be the same)

                        SimpleDateFormat newDateFormat = new SimpleDateFormat(TAG_DATE_FORMAT);
                        String dateString = parser.getAttributeValue(0);

                        dateString = dateString.replaceAll("/",".");

                        mDateUp = newDateFormat.parse(dateString).getTime();
                    }
                }
                eventType = parser.next();
            }
        }
        catch (XmlPullParserException | IOException | ParseException | java.text.ParseException e) {
            Logger.error(TAG, e);
        }
        return resultList;
    }

    // PARSE VALUTE TAG_CONTAINER
    private  ValuteDataParcel  readValute (XmlPullParser parser) throws IOException, XmlPullParserException {

        String id = parser.getAttributeValue(0);
        int numcode = 0;
        String charcode = "";
        int nominal = 0;
        String name = "";
        double value = 0;

        parser.require(XmlPullParser.START_TAG, null, TAG_VALUTE);

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nameParser = parser.getName();

            if (nameParser.equals(TAG_NUMCODE))
            {
                numcode = Integer.parseInt(getTagValue(parser, TAG_NUMCODE));
            }
            else if (nameParser.equals(TAG_CHARCODE))
            {
                charcode = getTagValue(parser, TAG_CHARCODE);
/** BECAUSE "TRY" CAN'T BE A FILENAME &  TRL - old version for this charcode*/
                if(charcode.equals("TRY"))
                {
                    charcode = "TRL";
                }
            }
            else if (nameParser.equals(TAG_NOMINAL))
            {
                nominal = Integer.parseInt(getTagValue(parser, TAG_NOMINAL));
            }
            else if (nameParser.equals(TAG_NAME_VALUTE))
            {
                name = getTagValue(parser, TAG_NAME_VALUTE);
            }
            else if (nameParser.equals(TAG_VALUE))
            {
                value = Double.parseDouble(getTagValue(parser, TAG_VALUE).replace(",","."));
            }
        }
        return new ValuteDataParcel(id, numcode, charcode, nominal, name, value, mDateUp);
    }

    // RETURN VALUE FOR TAG
    private  String getTagValue(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException {

        String result = "";

        parser.require(XmlPullParser.START_TAG, null, tagName);

        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }

        parser.require(XmlPullParser.END_TAG, null, tagName);
        return result;
    }

}
