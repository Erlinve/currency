/**
 * © Erlinve.ru 2016
 * LIcense ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;

import ru.erlinve.currency.IMainService;
import ru.erlinve.currency.service.MainService;

public abstract class BaseManageActivity extends AppCompatActivity {

    private static final String TAG = BaseManageActivity.class.getName();

    protected abstract void postServiceConnect();
    protected abstract void preServiceDisconnect();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMainService == null)
        {
            Intent i = new Intent(this, MainService.class);
            bindService(i, mServiceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onPause()
    {
        preServiceDisconnect();
        if(mMainService !=null)
        {
            unbindService(mServiceConnection);
            mMainService = null;
        }
        super.onPause();
    }

/*
********** SERVICE CONNECTING ***********
*/
    private IMainService mMainService = null;

    private final ServiceConnection mServiceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            mMainService = IMainService.Stub.asInterface(service);
            postServiceConnect();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            preServiceDisconnect();
            mMainService = null;
            unbindService(this);
        }
    };

    protected IMainService getMainService()
    {
        return mMainService;
    }
}
