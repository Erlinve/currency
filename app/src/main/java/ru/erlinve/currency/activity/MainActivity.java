/**
 * © Erlinve.ru 2016
 * LIcense ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.erlinve.currency.IMainService;
import ru.erlinve.currency.IServiceListener;
import ru.erlinve.currency.Logger;
import ru.erlinve.currency.R;
import ru.erlinve.currency.fragment.DinamicValuteFragment;
import ru.erlinve.currency.fragment.ListValuteFragment;
import ru.erlinve.currency.fragment.MsgDialogFragment;
import ru.erlinve.currency.fragment.StartFragment;
import ru.erlinve.currency.parcels.ValuteDataParcel;
import ru.erlinve.currency.parcels.ValuteDinamicParcel;

public class MainActivity extends BaseManageActivity implements FragmentManager.OnBackStackChangedListener {

    private static final String TAG = MainActivity.class.getName();

    private static final String TAG_LIST_FRAGMENT = "taglistfragment";
    private static final String TAG_DINAMIC_FRAGMENT = "tagdinamicfragment";
    private static final String TAG_START_FRAGMENT = "Startfragment";
    private static final String TAG_DIALOG_FRAGMENT = "msgdialog";

    private FragmentManager mFragmentManager;
    private ListValuteFragment mListValuteFragment;
    private DinamicValuteFragment mDinamicValuteFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentManager = this.getFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(this);
        this.createStartFragment();

        mListValuteFragment = (ListValuteFragment) mFragmentManager.findFragmentByTag(TAG_LIST_FRAGMENT);
        if(mListValuteFragment==null) {
            mListValuteFragment = new ListValuteFragment();
            mListValuteFragment.setRetainInstance(true);
        }
        mDinamicValuteFragment = (DinamicValuteFragment) mFragmentManager.findFragmentByTag(TAG_DINAMIC_FRAGMENT);
        if(mDinamicValuteFragment==null) {
            mDinamicValuteFragment = new DinamicValuteFragment();
            mDinamicValuteFragment.setRetainInstance(true);
        }
    }

/**
 *********************** SERVICE_LISTENER IMPLEMENTS *********************************
*/
private IServiceListener mServiceListener = new IServiceListener.Stub() {

    @Override
    public void handleValutaList(final List<ValuteDataParcel> listValuteData) {

        final List<ValuteDataParcel> data = listValuteData;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction ft = mFragmentManager.beginTransaction();
                mListValuteFragment = ListValuteFragment.newInstance(data);
                ft.replace(R.id.fragment_container, mListValuteFragment, TAG_LIST_FRAGMENT);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }

    @Override
    public void  handleErrorServer( final int errorCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                MsgDialogFragment msgDialog = MsgDialogFragment.newInstance(MsgDialogFragment.INTERNET_ERROR);
                msgDialog.show(mFragmentManager, TAG_DIALOG_FRAGMENT);
            }
        });
    }

    @Override
    public void handleValutaDinamic(List<ValuteDinamicParcel> listValuteData, final boolean fullRange) {

        final List<ValuteDinamicParcel> data = listValuteData;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDinamicValuteFragment.createGraphic((ArrayList) data, fullRange);
            }
        });
    }

};

/**
 ********************* IMPLEMENT FORM BASE_MANAGE_ACTIVITY ****************************
 */

    @Override
    protected void postServiceConnect() {

        IMainService mainService = getMainService();
        if (mainService != null) {
            try
            {
                mainService.startBackgroundThread(mServiceListener);
                if((!mDinamicValuteFragment.isVisible())
                        && (!mListValuteFragment.isVisible())) {
                    mainService.downloadValuteList(new Date().getTime());
                }
            }
            catch (RemoteException e) {
                Logger.error( TAG, e);
            }
        }
    }

    @Override
    protected void preServiceDisconnect() {

        IMainService mainService = getMainService();
        if(mainService!=null) {
            try {
                mainService.stopBackgroundThread();
            } catch (RemoteException e) {
                Logger.error(TAG, e);
            }
        }
    }

    public void downloadValuteList(long date) {

        IMainService mainService = getMainService();
        if(mainService != null) {
            try {
                mainService.downloadValuteList(date);
            } catch (RemoteException e) {
                Logger.error( TAG, e);
            }
        }
    }

    public void addDinamicFragment(ValuteDataParcel valuteDataParcel) {

        FragmentTransaction ft = mFragmentManager.beginTransaction();
        mDinamicValuteFragment = DinamicValuteFragment.newInstance(valuteDataParcel);

        ft.replace(R.id.fragment_container, mDinamicValuteFragment, TAG_DINAMIC_FRAGMENT);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void downloadValuteDinamic(ValuteDataParcel valuteDataParcel, long dateBegin, long dateEnd) {

        IMainService mainService = getMainService();

        if (mainService != null) {

            try {
                mainService.downloadValuteDinamic(dateBegin, dateEnd, valuteDataParcel.getId());
            } catch (RemoteException e) {
                Logger.error(TAG, e);
            }
        }
        else
        {
            new AsyncTask<Integer, Integer , Integer >() {
                @Override
                protected Integer doInBackground(Integer... params) {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Logger.error(TAG, e.toString(), e);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mDinamicValuteFragment.onRefresh();
                        }
                    });
                    return null;
                }
            }.execute(0, 0, 0);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackStackChanged() {

        if((mFragmentManager.getBackStackEntryCount())<=0)
        {
            this.finish();
        }
    }

    private void createStartFragment()
    {
        if(mFragmentManager.getBackStackEntryCount()==0)
        {
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            StartFragment startFragment = StartFragment.newInstance();
            ft.replace(R.id.fragment_container, startFragment, TAG_START_FRAGMENT);
            ft.commit();
        }
    }
}
