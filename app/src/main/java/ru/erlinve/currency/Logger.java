/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency;

import android.util.Log;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class Logger {
    private static final String TAG = Logger.class.getName();

    public static void create(Application app)
    {
        if(BuildConfig.METRICA_LOGS) {

            YandexMetrica.activate(app.getApplicationContext(),
                    YandexMetricaConfig.newConfigBuilder(
                            app.getApplicationContext().getString(R.string.Y_METRICA_ID)).build()
            );
            YandexMetrica.enableActivityAutoTracking(app);
        }
    }

/** *********************** ERROR FUNCTIONS **************** */

    public static void error(String tag, String message, Throwable throwable)
    {
        if(BuildConfig.METRICA_LOGS) {
            YandexMetrica.reportError("TAG: " + tag + " MESSAGE: " + message, throwable);
        }
        else {
            Log.e(tag, " MESSAGE: " + message +" THROWABLE: "+throwable.toString());
        }
    }
    public static void error(String tag, Throwable throwable)
    {
        if(BuildConfig.METRICA_LOGS) {
            YandexMetrica.reportError("TAG: "+tag, throwable);
        }
        else {
            Log.e(tag, " MESSAGE: " + throwable.getMessage() +" THROWABLE: "+throwable.toString());
        }
    }

    /** *********************** EVENT FUNCTIONS **************** */

    public static void event(String message)
    {
        if(BuildConfig.METRICA_LOGS) {
            YandexMetrica.reportEvent(message);
        }
        else {
            Log.e("CURRENCY_EVENT:", message);
        }
    }

    public static void event(String tag, String message)
    {
        if(BuildConfig.METRICA_LOGS) {
            YandexMetrica.reportEvent("TAG: "+tag+" MESSAGE: "+message);
        }
        else {
            Log.e(tag, message);
        }
    }

    /** *********************** LOCAL DEBUG FUNCTIONS **************** */

    public static void debug(String tag, String message)
    {
        if(!BuildConfig.METRICA_LOGS) {
            Log.d(tag, message);
        }
    }
}
