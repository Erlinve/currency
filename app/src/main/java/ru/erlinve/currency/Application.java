/**
 * © Erlinve.ru 2016
 * LIcense ~/Currency/LICENSE (GPL2)
 */

package ru.erlinve.currency;

public class Application extends android.app.Application {

    private static final String TAG = Application.class.getName();

    @Override
    public void onCreate()
    {
        super.onCreate();
        Logger.create(this);
    }
}
