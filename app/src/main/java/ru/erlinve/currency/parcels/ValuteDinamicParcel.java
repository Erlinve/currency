/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.parcels;

import android.os.Parcel;
import android.os.Parcelable;

public class ValuteDinamicParcel implements Parcelable {

    private static final String TAG = ValuteDinamicParcel.class.getName();

    private String mId;
    private double mValue;
    private int mNominal;
    private long mDateUp;

/** CONSTRUCTOR */

    public ValuteDinamicParcel(String id, double val, int nominal, long date) {

        mId = id;
        mValue = val;
        mNominal = nominal;
        mDateUp = date;
    }

    public ValuteDinamicParcel(Parcel in) {

        this.readFromParcel(in);
    }

    /** SET PARAMETER FUNCTION */

    public void setId(String id) { mId = id; }
    public void setValue(double value) { mValue = value; }
    public void setNominal(int nominal) { mNominal = nominal; }
    public void setDateUp(long date) { mDateUp = date; }

    /** GET PARAMETER FUNCTION */

    public String getId() { return mId; }
    public double getValue() { return mValue; }
    public int getNominal() { return mNominal; }
    public long getDateUp() { return mDateUp; }

    public void readFromParcel( Parcel in) {

        mId = in.readString();
        mValue = in.readDouble();
        mNominal = in.readInt();
        mDateUp = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(mId);
        dest.writeDouble(mValue);
        dest.writeInt(mNominal);
        dest.writeLong(mDateUp);
    }

    public static final Creator<ValuteDinamicParcel> CREATOR = new Creator<ValuteDinamicParcel>() {
        @Override
        public ValuteDinamicParcel createFromParcel(Parcel in) {
            return new ValuteDinamicParcel(in);
        }

        @Override
        public ValuteDinamicParcel[] newArray(int size) {
            return new ValuteDinamicParcel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


}
