/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.service;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;

import ru.erlinve.currency.IServiceListener;
import ru.erlinve.currency.Logger;
import ru.erlinve.currency.parcels.ValuteDataParcel;
import ru.erlinve.currency.parcels.ValuteDinamicParcel;
import ru.erlinve.currency.xmlParser.ParserXmlDinamic;
import ru.erlinve.currency.xmlParser.ParserXmlList;

public class BackgroundThread extends HandlerThread {

    private static final String TAG = BackgroundThread.class.getName();

    private static final String URL_PREFORM_LIST = "http://www.cbr.ru/scripts/XML_daily_eng.asp?date_req=%1$td/%1$tm/%1$tY";
    private static final String URL_PREFORM_DINAMIC = "http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=%1$td/%1$tm/%1$tY&date_req2=%2$td/%2$tm/%2$tY&VAL_NM_RQ=%3$s";
    private static final String DATE_FORMAT_URL = "dd/MM/yyyy";

    private static final int INDEX_LOAD_DATA_LIST = 0;
    private static final int INDEX_PARSE_DATA_LIST = 1;
    private static final int INDEX_LOAD_DATA_DINAMIC = 2;
    private static final int INDEX_PARSE_DATA_DINAMIC = 3;
    private static final int INDEX_ERROR_EXCEPTION = 9;

    private volatile IServiceListener mServiceListener;
    private Handler mBackgroundHandler;

    public BackgroundThread(String name, IServiceListener serviceListener) {
        super(name);

        this.setPriority(Process.THREAD_PRIORITY_BACKGROUND);

        mServiceListener = serviceListener;

    }
/**
 ****************************** THREAD LOOP FUNCTIONS **********************
 **/
    public void prepareHandler() {

        mBackgroundHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                switch (msg.what) {

                    case INDEX_LOAD_DATA_LIST:
                        try {
                            loadDataList((long)msg.obj);
                        } catch (RemoteException | InterruptedException | IOException e) {

                            Logger.error(TAG, "DOWNLOAD_DATA_LIST",e);
                            mBackgroundHandler.obtainMessage(INDEX_ERROR_EXCEPTION, INDEX_LOAD_DATA_LIST).sendToTarget();
                        }
                        break;

                    case INDEX_PARSE_DATA_LIST:

                        try {
                            parseDataList((InputStream) msg.obj);
                        } catch (RemoteException | InterruptedException e) {

                            Logger.error(TAG, "PARCE_DATA_LIST", e);
                            mBackgroundHandler.obtainMessage(INDEX_ERROR_EXCEPTION, INDEX_PARSE_DATA_LIST).sendToTarget();
                        }
                        break;

                    case INDEX_LOAD_DATA_DINAMIC:

                        try {
                            loadDataDinamic((String) msg.obj);
                        } catch (IOException e) {
                            Logger.error(TAG, "DOWNLOAD_DATA_DINAMIC",e);
                            mBackgroundHandler.obtainMessage(INDEX_ERROR_EXCEPTION, INDEX_LOAD_DATA_DINAMIC).sendToTarget();
                        }
                        break;

                    case INDEX_PARSE_DATA_DINAMIC:

                        try {
                            parseDataDinamic((InputStream) msg.obj);
                        } catch (RemoteException e) {
                            Logger.error(TAG, "PARCE_DATA_DINAMIC",e);
                            mBackgroundHandler.obtainMessage(INDEX_ERROR_EXCEPTION, INDEX_PARSE_DATA_DINAMIC).sendToTarget();
                        }
                        break;

                    case INDEX_ERROR_EXCEPTION:
                        try {
                            handleError(msg.arg1);
                        } catch (RemoteException e) {
                            Logger.error(TAG, "HANDLE_ERROR", e);
                        }
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean quit () {

        mBackgroundHandler.removeCallbacksAndMessages(null);

        return super.quit();
    }

/**
 ********************** PUBLIC CALLS *******************************
 **/

    public void downloadValuteList(long date) {

        mBackgroundHandler.obtainMessage(INDEX_LOAD_DATA_LIST, date).sendToTarget();
    }

    public void downloadValuteDinamic(long dateBegin, long dateEnd, String valuteId) {

        if(Character.isLetter(valuteId.charAt(valuteId.length()-1))) {

            valuteId = valuteId.substring(0, valuteId.length()-1);
        }
        if(dateBegin>dateEnd)
        {
            long temp = dateBegin;
            dateBegin = dateEnd;
            dateEnd = temp;
        }

        String finalUrlString = new Formatter().format(URL_PREFORM_DINAMIC,
                                                        new Date((dateBegin)),
                                                        new Date((dateEnd)),
                                                        valuteId).toString();

        mBackgroundHandler.obtainMessage(INDEX_LOAD_DATA_DINAMIC, finalUrlString).sendToTarget();
    }

/**
 ************************ LOOPER'S FUNCTIONS *****************************
 **/

    private void handleError(int errorCode) throws RemoteException {

        mServiceListener.handleErrorServer(errorCode);
    }

    private void loadDataList(long date) throws InterruptedException, RemoteException, IOException {

        String finalUrlString = new Formatter().format(URL_PREFORM_LIST, new Date(date)).toString();
//        InputStream stream = null;
        InputStream stream = new XmlHttpLoader().getInputStream(finalUrlString);

        mBackgroundHandler.obtainMessage(INDEX_PARSE_DATA_LIST, stream).sendToTarget();
    }

    private void parseDataList(InputStream stream) throws InterruptedException, RemoteException {

        ParserXmlList parserXml = new ParserXmlList();

        List<ValuteDataParcel> data = parserXml.getListValuteParcel(stream);

        mServiceListener.handleValutaList(data);

    }

    private void loadDataDinamic(String url) throws IOException {

        InputStream stream = new XmlHttpLoader().getInputStream(url);

        mBackgroundHandler.obtainMessage(INDEX_PARSE_DATA_DINAMIC, stream).sendToTarget();

    }

    private void parseDataDinamic(InputStream stream) throws RemoteException {

        ParserXmlDinamic parserXml = new ParserXmlDinamic();

        List<ValuteDinamicParcel> data = parserXml.getListValuteParcel(stream);

        int mainNominal = data.get(data.size()-1).getNominal();
        String mainId = data.get(data.size()-1).getId();

        Iterator<ValuteDinamicParcel> iterator = data.iterator();
        boolean fullRange = true;
        while (iterator.hasNext())
        {
            ValuteDinamicParcel val = iterator.next();

            if(!val.getId().equals(mainId))
            {
                iterator.remove();
                fullRange = false;

            }
            else if(val.getNominal()!=mainNominal)
            {
                val.setValue(val.getValue()*mainNominal/val.getNominal());
                val.setNominal(mainNominal);
            }
        }

        mServiceListener.handleValutaDinamic(data, fullRange);

    }
}
