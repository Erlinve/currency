/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */

package ru.erlinve.currency.service;

import android.content.Context;

import ru.erlinve.currency.IMainService;
import ru.erlinve.currency.IServiceListener;
import ru.erlinve.currency.Logger;

public class MainServiceRealization extends IMainService.Stub {

    private static final String TAG = MainServiceRealization.class.getName();
    private static final String TAG_THREADNAME = "CurrencyThread";

    private BackgroundThread mBackgroundThread;

    public MainServiceRealization (Context context) {

    }

    @Override
    public void startBackgroundThread(IServiceListener listener) {

        mBackgroundThread = new BackgroundThread(TAG_THREADNAME, listener);
        mBackgroundThread.start();
        mBackgroundThread.prepareHandler();

    }

    @Override
    public void stopBackgroundThread () {

        mBackgroundThread.quit();
        mBackgroundThread.interrupt();
    }

    @Override
    public void downloadValuteList(long date) {

        if(mBackgroundThread.isAlive()) {

            mBackgroundThread.downloadValuteList(date);
        }
    }

    @Override
    public void downloadValuteDinamic(long dateBegin, long dateEnd, String valuteId) {

        if(mBackgroundThread.isAlive()) {

            mBackgroundThread.downloadValuteDinamic(dateBegin, dateEnd, valuteId);
        }
    }

}
