/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import ru.erlinve.currency.IMainService;

public class MainService extends Service {
    private static final String TAG = MainService.class.getName();

    private IMainService.Stub mBinder = null;

    private IMainService.Stub getBinder() {

        if(mBinder ==null)
        {

            mBinder = new MainServiceRealization(getBaseContext());
        }
        return mBinder;
    }

    @Override
    public IBinder onBind(Intent intent) {

        return getBinder();
    }

    @Override
    public void onCreate()
    {
    }

    @Override
    public void onDestroy()
    {
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        return super.onUnbind(intent);
    }
}
