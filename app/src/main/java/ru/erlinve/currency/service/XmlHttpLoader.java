/**
 * © Erlinve.ru 2016
 * License ~/Currency/LICENSE (GPL2)
 */
package ru.erlinve.currency.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class XmlHttpLoader {

    private static final String TAG = XmlHttpLoader.class.getName();
    private static final String TAG_GET_METHOD = "GET";

    public XmlHttpLoader() {
    }

    public InputStream getInputStream(String urlString) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod(TAG_GET_METHOD);
        conn.setDoInput(true);

        conn.connect();

        return conn.getInputStream();

    }
}
