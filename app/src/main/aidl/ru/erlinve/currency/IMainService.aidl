/**
* © Erlinve.ru 2016
* LIcense ~/Currency/LICENSE (GPL2)
*/

package ru.erlinve.currency;

import ru.erlinve.currency.IServiceListener;

interface IMainService {

    oneway void startBackgroundThread(in IServiceListener listener);
    oneway void stopBackgroundThread();
    oneway void downloadValuteList(long date);
    oneway void downloadValuteDinamic(long dateBegin, long dateEnd, String valuteId);
}
