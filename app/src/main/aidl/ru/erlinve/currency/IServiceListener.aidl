/**
* © Erlinve.ru 2016
* LIcense ~/Currency/LICENSE (GPL2)
*/

package ru.erlinve.currency;

import ru.erlinve.currency.parcels.ValuteDataParcel;
import ru.erlinve.currency.parcels.ValuteDinamicParcel;

interface IServiceListener {

    void handleValutaList( in List<ValuteDataParcel> listValuteData);
    void handleValutaDinamic( in List<ValuteDinamicParcel> listValuteData, boolean fullRange);
    void handleErrorServer(in int errorCode);
}
